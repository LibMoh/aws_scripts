\#!/bin/bash
#set -xv
data=$(./start_ubuntu libanvmdelete inginx)
IPAddr=$(echo "$data" | jq -r '.Instances[] | .PrivateIpAddress')
echo "DEBUG IP: $IPAddr"
ids=$(echo "$data" | jq -r '.Instances[] | .InstanceId')
counter=0
while ! curl -s --connect-timeout 5 http://$IPAddr
do
	echo "DEBUG counter: $counter"
	sleep 30
	((counter=counter + 1))
	if (( counter == 6 ))
	then
		exit 1
	fi
done
if curl -s --connect-timeout 5 http://$IPAddr | grep "Hello Lee"
then
	echo "OK"
	echo "Creating Image"

        imgdata=$(aws ec2 create-image --instance-id $ID --name LibanUbuntuNginxImage --region eu-west-1)
        imgID=$(echo "$imgdata" | jq -r '.ImageId')
        echo "Image ID: $imgID"
        while ! aws ec2 describe-images --image-ids $imgID | jq -r '.Images[] | .State' | grep available
        do
                sleep 30
        done
        echo "Image built"
        aws ec2 terminate-instances --instance-ids $ids --region eu-west-1

	exit 0
else

	echo "NOK"
	aws ec2 terminate-instances --instance-ids $ids --region eu-west-1
	exit 1
fi

